#pragma once

#include <math.h>

#include "SDL.h"
#include "Object.h"

class Paddle : public Object
{
public:
	Paddle(int x, int y, std::string texturePath);

	static const int PADDLE_VEL = 10;

	void handleEvents(SDL_Event &e);
	void move();

	virtual int newVelocityX(int ballPosX, int actVelX, int oldVelX) override;

private:
	int velX;
};

