#include "Ball.h"

#define NEGATIVE -10;

Ball::Ball(int x, int y, std::string texturePath)
	: Object(x, y, texturePath)
{
	isMoving = false;

	velX = 0;
	velY = 0;

	texture.loadFromFile(texturePath);

	//Nulling the collider
	collider.x = NEGATIVE;
	collider.y = NEGATIVE;
	collider.w = 0;
	collider.h = 0;
}

void Ball::startMove()
{
	if (!isMoving)
	{
		isMoving = true;
		velX = BALL_X_VEL;
		velY = BALL_Y_VEL;
	}
}

void Ball::move(std::vector<Object *> &objects, Paddle &paddle)
{
	if (isMoving)
	{
		x += velX;
		if (x < 0 || x + width > WINDOW_WIDTH)
		{
			x -= velX;
			velX = -velX;
		}
		else
		{
			int didCollide = checkCollision(objects);
			if (didCollide < objects.size())
			{
				x -= velX;
				velX = -velX;

				if (objects[didCollide]->handleCollision())
				{
					objects.erase(objects.begin() + didCollide);
				}
			}
		}

		y += velY;
		if (y < 0)
		{
			y -= velY;
			velY = -velY;
		}
		else if (y + height > WINDOW_HEIGHT)
		{
			isMoving = false;
		}
		else
		{
			int didCollide = checkCollision(objects);
			if (didCollide < objects.size())
			{
				y -= velY;
				velY = -velY;

				velX = objects[didCollide]->newVelocityX(x, velX, BALL_X_VEL);
				
				if (objects[didCollide]->handleCollision())
				{
					objects.erase(objects.begin() + didCollide);
				}
			}
		}
	}
	else
	{
		x = paddle.getX() + (paddle.getWidth() / 2);
		y = paddle.getY() - height;
	}
}

int Ball::checkCollision(std::vector<Object *> &objects)
{
	std::vector<Object *>::iterator obj = objects.begin();
	int i = 0;

	for(; obj != objects.end(); )
	{
		SDL_Rect *col = (*obj)->getCollider();
		if (!(y + height <= col->y || y >= col->y + col->h || x + width <= col->x || x >= col->x + col->w))
		{
			//printf("COLLIDES %d\n", i);
			return i;
		}
		else
		{
			obj++;
			i++;
		}
	}

	return i;
}
