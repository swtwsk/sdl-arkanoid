#include <cstdio>
#include <string>
#include <vector>
#include "SDL.h"
#include "SDL_image.h"

#include "Globals.h"
#include "Render.h"
#include "Object.h"
#include "Ball.h"
#include "Brick.h"
#include "Paddle.h"

/*
* Initialize SDL library and create window.
* Returns false if failed to initialize.
*/
bool init(Render &render)
{
	if (!render.checkInit())
	{
		printf("Rendering could not be initialized!\n");
		return false;
	}

	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf("SDL_image could not be initialized. SDL_image Error: %s\n", IMG_GetError());
		return false;
	}

	return true;
}

/*
* Deallocate memory and exit
*/
/*void close(SDL_Window **window, SDL_Renderer **renderer)
{
	SDL_DestroyRenderer(*renderer);
	SDL_DestroyWindow(*window);
	renderer = NULL;
	window = NULL;

	IMG_Quit();
	SDL_Quit();
}*/

void putBricks(std::vector <Brick> &bricks)
{
	int i = 0;

	for (int y = 25; y <= WINDOW_HEIGHT / 2; y += 30)
	{
		for (int x = 25; x + 50 <= WINDOW_WIDTH - 25; x += 50)
		{
			if (i == 5)
			{
				bricks.push_back(Brick(x, y, "Sprites/Bricks/brick.png", 3));
				i = 0;
			}
			else
			{
				//Brick brick(x, y, "Sprites/Bricks/brick.png");
				bricks.push_back(Brick(x, y, "Sprites/Bricks/brick.png"));
			}
			i++;
		}
	}
}

int main(int argc, char **argv)
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not be initialized. SDL Error: %s\n", SDL_GetError());
		return false;
	}

	SDL_Event event;
	Render render("Sprites/Background/background.png");

	if (!init(render))
	{
		printf("Failed to initialize\n");
		exit(1);
	}

	bool gameRunning = true;

	std::vector<Object *> objects;

	Ball ball(WINDOW_WIDTH / 2, WINDOW_HEIGHT - 70, "Sprites/Balls/ball.png");
	objects.push_back(&ball);

	Paddle paddle((WINDOW_WIDTH - 60) / 2, WINDOW_HEIGHT - 50, "Sprites/paddle.png");
	objects.push_back(&paddle);

	/*Brick brick(100, 100, "Sprites/Bricks/brick.png");
	objects.push_back(&brick);

	Brick secondBrick(400, 100, "Sprites/Bricks/brick.png", 3);
	objects.push_back(&secondBrick);*/
	std::vector <Brick> bricks;
	putBricks(bricks);

	for (Brick &brick : bricks)
	{
		objects.push_back(&brick);
	}

	while (gameRunning)
	{
		while (SDL_PollEvent(&event) != 0)
		{
			if (event.type == SDL_QUIT)
			{
				gameRunning = false;
			}
			else if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					ball.startMove();
					break;
				default:
					break;
				}
			}

			paddle.handleEvents(event);
		}

		paddle.move();
		ball.move(objects, paddle);
		
		render.render(objects);
		//}

		/*SDL_RenderClear(renderer);

		SDL_RenderCopy(renderer, background, NULL, NULL);

		SDL_RenderPresent(renderer);*/
	}

	//close(&window, &renderer);
	return 0;
}