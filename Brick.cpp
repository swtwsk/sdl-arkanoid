#include "Brick.h"

Brick::Brick(int x, int y, std::string texturePath, int maxBrickLife) : Object(x, y, texturePath)
{
	brickLife = maxBrickLife;
	this->maxBrickLife = maxBrickLife;
}

void Brick::setNewTexture()
{
	std::string newTexturePath = texturePath.substr(0, texturePath.length() - 4) + "_cracked.png";
	texture.loadFromFile(newTexturePath.c_str());
}

bool Brick::handleCollision()
{
	if (--brickLife <= 0)
	{
		free();
		return true;
	}
	else if (brickLife == maxBrickLife / 2)
	{
		setNewTexture();
	}
	
	return false;
}