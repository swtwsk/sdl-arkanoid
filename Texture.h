#pragma once

#include <string>
#include "SDL.h"
#include "SDL_image.h"
//#include "SDL_ttf.h"

#include "Globals.h"

class Texture
{
public:
	//Constructor & destructor
	Texture();
	~Texture();

	//Loads an image at specified path
	bool loadFromFile(std::string path);

	//Creates image from font string
	//bool loadFromRenderedText(TTF_Font *font, std::string textureText, SDL_Color textColor, SDL_Renderer *renderer);

	//Deallocates texture
	void free();

	//Renders texture at given point
	void render(int x, int y, SDL_Rect *clip = NULL, double angle = 0.0,
		SDL_Point *center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//Gets image dimensions
	int getWidth();
	int getHeight();

	bool isTexture();
private:
	SDL_Texture *texture;

	int width;
	int height;
};