#include "Object.h"

Object::Object(int x, int y, std::string texturePath) : x(x), y(y), texturePath(texturePath)
{
	texture.loadFromFile(texturePath);

	this->height = texture.getHeight();
	this->width = texture.getWidth();

	collider.x = x;
	collider.y = y;
	collider.w = width;
	collider.h = height;
}

Object::~Object()
{
	free();
}

Object::Object(const Object & that) : x(that.x), y(that.y), texturePath(that.texturePath)
{
	texture.loadFromFile(that.texturePath);

	this->height = texture.getHeight();
	this->width = texture.getWidth();

	collider.x = x;
	collider.y = y;
	collider.w = width;
	collider.h = height;
}

Object &Object::operator=(const Object &that)
{
	if (this != &that)
	{
		x = that.x;
		y = that.y;
		width = that.width;
		height = that.height;

		collider.x = x;
		collider.y = y;
		collider.w = width;
		collider.h = height;

		texture.loadFromFile(that.texturePath);
		texturePath = that.texturePath;
	}

	return *this;
}

void Object::free()
{
	texture.free();
}

int Object::getX()
{
	return x;
}

int Object::getY()
{
	return y;
}

int Object::getWidth()
{
	return width;
}

int Object::getHeight()
{
	return height;
}

bool Object::handleCollision()
{
	return false;
}

int Object::newVelocityX(int ballPosX, int actVelX, int oldVelX)
{
	return actVelX;
}

void Object::render()
{
	texture.render(x, y);
}

SDL_Rect *Object::getCollider()
{
	return &collider;
}
