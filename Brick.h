#pragma once

#include "Object.h"
#include "SDL.h"

class Brick : public Object
{
public:
	Brick(int x, int y, std::string texturePath, int maxBrickLife = 1);

	virtual bool handleCollision() override;

private:
	int maxBrickLife;
	int brickLife;

	void setNewTexture();
};