#pragma once

#include "SDL.h"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

extern SDL_Window *window;
extern SDL_Renderer *renderer;