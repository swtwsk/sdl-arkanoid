#pragma once

#include <string>
#include <cstdio>
#include <vector>
#include "SDL.h"

#include "Globals.h"
#include "Object.h"
#include "Texture.h"

class Render
{
public:
	//Constructors
	Render();
	Render(std::string backgroundPath);
	~Render();

	//Check, if SDL has been initialized properly
	bool checkInit();

	//Render all what's needed XD
	void render(std::vector <Object *> objects);

private:
	Texture background;
};

