#include "Paddle.h"


Paddle::Paddle(int x, int y, std::string texturePath) : Object(x, y, texturePath)
{
	this->velX = 0;
}

void Paddle::handleEvents(SDL_Event &e)
{
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_LEFT: velX -= PADDLE_VEL; break;
		case SDLK_RIGHT: velX += PADDLE_VEL; break;
		}
	}
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_LEFT: velX += PADDLE_VEL; break;
		case SDLK_RIGHT: velX -= PADDLE_VEL; break;
		}
	}
}

void Paddle::move()
{
		x += velX;

		if (x < 0 || x + width > WINDOW_WIDTH) //|| checkCollision(objects))
		{
			x -= velX;
		}

		collider.x = x;
}

int Paddle::newVelocityX(int ballPosX, int actVelX, int oldVelX)
{
	int ballPos = width - (ballPosX - x);
	float angle = ballPos * (M_PI / width);

	int newVelX = (int) std::floorf(std::cosf(angle) * oldVelX);

	return (newVelX == 0 ? oldVelX : newVelX);
}
