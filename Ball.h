#pragma once

#include <string>
#include <vector>

#include "Globals.h"
#include "Object.h"
#include "Paddle.h"

class Ball : public Object
{
public:
	//static const int BALL_WIDTH = 20;
	//static const int BALL_HEIGHT = 20;

	static const int BALL_X_VEL = 2;
	static const int BALL_Y_VEL = 5;

	//Constructor
	Ball(int x, int y, std::string texturePath);
	//~Ball();

	//Moving functions
	void startMove();
	void move(std::vector<Object *>& objects, Paddle &paddle);

private:
	bool isMoving;

	int velX;
	int velY;

	//Colliders
	int checkCollision(std::vector<Object *> &objects);
};

