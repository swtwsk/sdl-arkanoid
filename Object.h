#pragma once

#include <string>
#include "SDL.h"

#include "Globals.h"
#include "Texture.h"

class Object
{
public:
	//Constructor
	Object(int x, int y, std::string texturePath);

	//Destructor
	~Object();

	//Copy constructor
	Object(const Object &that);

	//Copy assignment operator
	Object& operator=(const Object &that);

	//Frees alocated memory
	void free();

	int getX();
	int getY();

	int getWidth();
	int getHeight();

	//Checks if there was a collision and handles it
	virtual bool handleCollision();

	virtual int newVelocityX(int ballPosX, int actVelX, int oldVelX);

	void render();

	SDL_Rect *getCollider();

protected:
	//Object coordinates
	int x;
	int y;

	//Object size
	int width;
	int height;

	//Texture
	Texture texture;
	std::string texturePath;

	SDL_Rect collider;
};

