#include "Render.h"

Render::Render()
{
	window = SDL_CreateWindow("Arkanoid", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
}

Render::Render(std::string backgroundPath) : Render()
{
	background.loadFromFile(backgroundPath);
}

Render::~Render()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	renderer = NULL;
	window = NULL;
}

bool Render::checkInit()
{
	if (window == NULL)
	{
		printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	if (renderer == NULL)
	{
		printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	return true;
}

void Render::render(std::vector <Object *> objects)
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
	SDL_RenderClear(renderer);

	if (background.isTexture())
	{
		background.render(0, 0);
	}

	for (Object *o : objects)
	{
		o->render();
	}

	//SDL_RenderClear(renderer);
	//SDL_RenderCopy(renderer, background, NULL, NULL);

	SDL_RenderPresent(renderer);
}